﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Minipokedex.Common.Data;
using Minipokedex.Common.Entity;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;
using Minipokedex.Core.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace Minipokedex.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PokemonContext>(options => options
                .UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IGenericRepository<Pokemon>, GenericRepository<Pokemon>>();
            services.AddTransient<IGenericRepository<Generation>, GenericRepository<Generation>>();
            services.AddTransient<IGenericRepository<ElementType>, GenericRepository<ElementType>>();
            services.AddTransient<IGenericRepository<PokemonDetail>, GenericRepository<PokemonDetail>>();
            services.AddTransient<IGenericRepository<BaseStats>, GenericRepository<BaseStats>>();
            services.AddTransient<IGenericRepository<MoveSet>, GenericRepository<MoveSet>>();
            services.AddTransient<IGenericRepository<PokemonMoves>, GenericRepository<PokemonMoves>>();
            services.AddTransient<IPokemon, PokemonService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });

            });
            //CORS
            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = true;
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAngular",
                    builder => builder.WithOrigins("http://localhost:4200")
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, PokemonContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            DataSeeder.SeedGeneration(context);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("swagger/v1/swagger.json", "My API");
                c.RoutePrefix = string.Empty;
            });
            //CORS

            app.UseCors("AllowAngular");

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
