﻿using Minipokedex.Common.Entity;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Minipokedex.Core.Services
{
    public class PokemonService : IPokemon
    {
        private PokemonContext _pokemonContext;

        public PokemonService(PokemonContext pokemonContext)
        {
            _pokemonContext = pokemonContext;
        }

        public IEnumerable<ElementType> GetAllElement()
        {
            return _pokemonContext.ElementTypes.ToList();
        }

        public IEnumerable<Generation> GetAllGeneration()
        {
            return _pokemonContext.Generations.ToList();
        }

        public string GetElementName(string guid)
        {
            return _pokemonContext.ElementTypes.Where(x => x.Id == Guid.Parse(guid)).Select(y => y.ElementName).Single();
        }

        public IEnumerable<string> GetPokemonMoves(string guid)
        {
            var s = _pokemonContext.PokemonMoves.Where(x => x.PokemonId == Guid.Parse(guid)).Select(y => y.MoveSetId).ToList();

            List<string> toRetun = new List<string>();

            foreach (var item in s)
            {
                var v = _pokemonContext.MoveSets.Where(x => x.Id == item).Select(y => y.MoveName).Single();
                toRetun.Add(v);
            }
            return toRetun;
        }
    }
}
