﻿using Microsoft.EntityFrameworkCore;
using Minipokedex.Common.Entity;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Minipokedex.Common.Data
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseModel
    {
        private readonly PokemonContext _pokemonContext;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(PokemonContext pokemonContext)
        {
            _pokemonContext = pokemonContext ?? throw new ArgumentNullException("Context Chaina");
            _dbSet = _pokemonContext.Set<TEntity>();
        }

        public async Task<TEntity> AddEntityAsync(TEntity entityToAdd)
        {
            entityToAdd.CreatedDate = DateTime.Now;
            await _dbSet.AddAsync(entityToAdd);
            await _pokemonContext.SaveChangesAsync();
            return entityToAdd;
        }

        public async void AddEntityList(IEnumerable<TEntity> listOfEntity)
        {
            await _dbSet.AddRangeAsync(listOfEntity);
            await _pokemonContext.SaveChangesAsync();
        }

        public async Task<int> DeleteEntityAsync(TEntity entityToDelete)
        {
            _dbSet.Remove(entityToDelete);
            return await _pokemonContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<TEntity> GetEntityByIdAsync(Guid guid)
        {
            return await _dbSet.FindAsync(guid);
        }

        public async Task<TEntity> UpdateEntityAsync(TEntity entityToUpdate)
        {
            entityToUpdate.ModifiedDate = DateTime.Today.Date;
            _dbSet.Attach(entityToUpdate);
            _pokemonContext.Entry(entityToUpdate).State = EntityState.Modified;
            await _pokemonContext.SaveChangesAsync();
            return entityToUpdate;
        }
    }
}
