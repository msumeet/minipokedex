﻿using Minipokedex.Common.Entity;
using Minipokedex.Core.Enum;
using Minipokedex.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Minipokedex.Common.Data
{
    public class DataSeeder
    {
        private static DateTime today = DateTime.Now.Date;
        public static void SeedGeneration(PokemonContext context)
        {

            GenerationSeeder(context);
            ElementSeeder(context);

        }

        private async static void GenerationSeeder(PokemonContext context)
        {

            if (context.Generations.Count() == 0)
            {
                List<Generation> genList = new List<Generation>();
                Generation generation = new Generation
                {
                    Id = new Guid(),
                    GenerationName = "Kanto",
                    TotalPokemon = 151,
                    CreatedDate = today
                };
                genList.Add(generation);
                generation = new Generation
                {
                    Id = new Guid(),
                    GenerationName = "Johto",
                    TotalPokemon = 100,
                    CreatedDate = today
                };
                genList.Add(generation);
                generation = new Generation
                {
                    Id = new Guid(),
                    GenerationName = "Hoenn",
                    TotalPokemon = 211,
                    CreatedDate = today
                };
                genList.Add(generation);
                await context.AddRangeAsync(genList);
                await context.SaveChangesAsync();
            }

        }

        private async static void ElementSeeder(PokemonContext context)
        {
            if (context.ElementTypes.Count() == 0)
            {


                string[] cCode = { "7f7f7f", "d14165", "90aeb0", "9e68b6", "da7847", "6cb985", "91c326", "52634b", "54849a", "ff9d51", "4c92d8", "65b961", "f6d53a", "eb7571", "77c8bf", "1172d0", "5a5768", "ed90e0" };
                List<ElementType> elementTypeList = new List<ElementType>();
                for (int i = 1; i < 19; i++)
                {
                    Element elm = (Element)i;
                    string elmString = elm.ToString();
                    ElementType elementType = new ElementType()
                    {
                        Id = new Guid(),
                        ElementName = elmString,
                        CreatedDate = today,
                        ColorCode = cCode[i - 1]
                    };
                    elementTypeList.Add(elementType);
                }
                await context.ElementTypes.AddRangeAsync(elementTypeList);
                await context.SaveChangesAsync();
            }
        }
    }
}
