﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Minipokedex.Common.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseStats",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Attack = table.Column<int>(nullable: false),
                    Defence = table.Column<int>(nullable: false),
                    Stamina = table.Column<int>(nullable: false),
                    MaxCombatPower = table.Column<int>(nullable: false),
                    Height = table.Column<decimal>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseStats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ElementTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ElementName = table.Column<string>(nullable: true),
                    ColorCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Generations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    GenerationName = table.Column<string>(nullable: true),
                    TotalPokemon = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Generations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PokemonDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    TotalEvolution = table.Column<int>(nullable: false),
                    Info = table.Column<string>(nullable: true),
                    BaseStatsId = table.Column<Guid>(nullable: false),
                    BuddyDistance = table.Column<int>(nullable: false),
                    Obtainability = table.Column<int>(nullable: false),
                    EggGroup = table.Column<int>(nullable: false),
                    Family = table.Column<string>(nullable: true),
                    EvolutionStage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PokemonDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PokemonDetails_BaseStats_BaseStatsId",
                        column: x => x.BaseStatsId,
                        principalTable: "BaseStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MoveSets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    MoveName = table.Column<string>(nullable: true),
                    Damage = table.Column<string>(nullable: true),
                    DamagePerSecond = table.Column<decimal>(nullable: false),
                    ElementTypeId = table.Column<Guid>(nullable: false),
                    MoveType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoveSets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MoveSets_ElementTypes_ElementTypeId",
                        column: x => x.ElementTypeId,
                        principalTable: "ElementTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pokemons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    PokemonId = table.Column<int>(nullable: false),
                    PokemonName = table.Column<string>(nullable: true),
                    GenerationId = table.Column<Guid>(nullable: false),
                    ElementPrimary = table.Column<Guid>(nullable: false),
                    ElementSecondary = table.Column<Guid>(nullable: true),
                    PokemonDetailId = table.Column<Guid>(nullable: false),
                    AssetsLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pokemons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pokemons_Generations_GenerationId",
                        column: x => x.GenerationId,
                        principalTable: "Generations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pokemons_PokemonDetails_PokemonDetailId",
                        column: x => x.PokemonDetailId,
                        principalTable: "PokemonDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PokemonMoves",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    PokemonId = table.Column<Guid>(nullable: false),
                    MoveSetId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PokemonMoves", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PokemonMoves_MoveSets_MoveSetId",
                        column: x => x.MoveSetId,
                        principalTable: "MoveSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PokemonMoves_Pokemons_PokemonId",
                        column: x => x.PokemonId,
                        principalTable: "Pokemons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MoveSets_ElementTypeId",
                table: "MoveSets",
                column: "ElementTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MoveSets_MoveName",
                table: "MoveSets",
                column: "MoveName",
                unique: true,
                filter: "[MoveName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PokemonDetails_BaseStatsId",
                table: "PokemonDetails",
                column: "BaseStatsId");

            migrationBuilder.CreateIndex(
                name: "IX_PokemonMoves_MoveSetId",
                table: "PokemonMoves",
                column: "MoveSetId");

            migrationBuilder.CreateIndex(
                name: "IX_PokemonMoves_PokemonId",
                table: "PokemonMoves",
                column: "PokemonId");

            migrationBuilder.CreateIndex(
                name: "IX_Pokemons_GenerationId",
                table: "Pokemons",
                column: "GenerationId");

            migrationBuilder.CreateIndex(
                name: "IX_Pokemons_PokemonDetailId",
                table: "Pokemons",
                column: "PokemonDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_Pokemons_PokemonId",
                table: "Pokemons",
                column: "PokemonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pokemons_PokemonName",
                table: "Pokemons",
                column: "PokemonName",
                unique: true,
                filter: "[PokemonName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PokemonMoves");

            migrationBuilder.DropTable(
                name: "MoveSets");

            migrationBuilder.DropTable(
                name: "Pokemons");

            migrationBuilder.DropTable(
                name: "ElementTypes");

            migrationBuilder.DropTable(
                name: "Generations");

            migrationBuilder.DropTable(
                name: "PokemonDetails");

            migrationBuilder.DropTable(
                name: "BaseStats");
        }
    }
}
