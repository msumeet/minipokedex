﻿using Microsoft.EntityFrameworkCore;
using Minipokedex.Core.Model;

namespace Minipokedex.Common.Entity
{
    public class PokemonContext : DbContext
    {
        public PokemonContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<BaseStats> BaseStats { get; set; }
        //public DbSet<CounterVunerable> CounterNVunerable { get; set; }
        public DbSet<ElementType> ElementTypes { get; set; }
        public DbSet<Generation> Generations { get; set; }
        public DbSet<MoveSet> MoveSets { get; set; }
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<PokemonDetail> PokemonDetails { get; set; }
        public DbSet<PokemonMoves> PokemonMoves { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Pokemon>().HasIndex(x => x.PokemonId).IsUnique(true);
            modelBuilder.Entity<Pokemon>().HasIndex(x => x.PokemonName).IsUnique(true);

            modelBuilder.Entity<MoveSet>().HasIndex(x => x.MoveName).IsUnique(true);
        }
    }
}
