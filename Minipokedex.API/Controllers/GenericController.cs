﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Minipokedex.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAngular")]
    public class GenericController<T> : ControllerBase where T : BaseModel
    {
        private readonly IGenericRepository<T> _genericRepository;

        public GenericController(IGenericRepository<T> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        [HttpPost("Add")]
        public async Task<ActionResult<T>> AddEntityAsync([FromBody] T entityToAdd)
        {
            if (entityToAdd.Id != null)
            {
                try
                {
                    var result = await _genericRepository.AddEntityAsync(entityToAdd);
                    return Ok(result);
                }
                catch (Exception e)
                {
                    return BadRequest(e.InnerException.Message);
                    //throw;
                }
            }
            return BadRequest("Id cha paile nai");
        }

        [HttpPost("AddList")]
        public async Task<ActionResult> AddEntityList(IEnumerable<T> listOfEntity)
        {
            try
            {
                _genericRepository.AddEntityList(listOfEntity);
                return Ok("Successfully Inserted");
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpDelete("Delete")]
        public async Task<ActionResult<int>> DeleteEntityAsync([FromBody] T entityToDelete)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _genericRepository.DeleteEntityAsync(entityToDelete);
                    return Ok(result);
                }
                return NotFound("Cannot be empty");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet("GetById/{guid}")]
        public async Task<ActionResult<T>> GetEntityByIdAsync(Guid guid)
        {
            try
            {
                var result = await _genericRepository.GetEntityByIdAsync(guid);
                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult<T>> GetAllAsync()
        {
            try
            {
                var result = await _genericRepository.GetAllAsync();
                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPut("Update/{guid}")]
        public async Task<ActionResult<T>> UpdateAsync(string guid, [FromBody] T entityToUpdate)
        {
            try
            {
                entityToUpdate.Id = Guid.Parse(guid);
                entityToUpdate.ModifiedDate = DateTime.Now;
                var result = await _genericRepository.UpdateEntityAsync(entityToUpdate);
                return Ok(result);

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
