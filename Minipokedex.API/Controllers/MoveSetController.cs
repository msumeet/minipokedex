﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Minipokedex.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAngular")]
    public class MoveSetController : GenericController<MoveSet>
    {
        public MoveSetController(IGenericRepository<MoveSet> genericRepository) : base(genericRepository)
        {
        }
    }
}
