﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Minipokedex.Core.Interface;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Minipokedex.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAngular")]
    public class CommonController : ControllerBase
    {
        private readonly IPokemon _pokemon;

        public CommonController(IPokemon pokemon)
        {
            _pokemon = pokemon;
        }

        [HttpGet("GetAllElem")]
        public ActionResult GetAllElement()
        {
            var result = _pokemon.GetAllElement();
            return Ok(result);
        }
        [HttpGet("GetAllGen")]
        public ActionResult GetAllGeneration()
        {
            var result = _pokemon.GetAllGeneration();
            return Ok(result);
        }

        [HttpGet("GetPokeMove/{guid}")]
        public ActionResult GetPokemonMove(string guid)
        {
            var result = _pokemon.GetPokemonMoves(guid);
            return Ok(result);
        }

        [HttpGet("GetElement/{guid}")]
        public ActionResult GetElement(string guid)
        {
            if (guid != null)
            {
                var result = _pokemon.GetElementName(guid);
                return Ok(result);
            }
            return BadRequest();
        }
    }
}
