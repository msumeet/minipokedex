﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Minipokedex.Core.Interface;
using Minipokedex.Core.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Minipokedex.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAngular")]
    public class PokemonMoveController : GenericController<PokemonMoves>
    {
        public PokemonMoveController(IGenericRepository<PokemonMoves> genericRepository) : base(genericRepository)
        {
        }
    }
}
