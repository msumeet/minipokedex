﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minipokedex.Core.Enum
{
    public enum Element
    {
        Normal=1,
        Fighting,
        Flying,
        Poision,
        Ground,
        Rock,
        Bug,
        Ghost,
        Steel,
        Fire,
        Water,
        Grass,
        Electric,
        Psychic,
        Ice,
        Dragon,
        Dark,
        Fairy
    }
}
