﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minipokedex.Core.Enum
{
    public enum Obtainability
    {
        Obtainable=1,
        UnObtainable
    }
}
