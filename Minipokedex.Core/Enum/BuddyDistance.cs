﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minipokedex.Core.Enum
{
    public enum BuddyDistance
    {
        OneKM=1,
        ThreeKM=3,
        FiveKM=5,
        TwentyKM=20
    }
}
