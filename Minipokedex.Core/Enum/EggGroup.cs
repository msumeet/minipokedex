﻿namespace Minipokedex.Core.Enum
{
    public enum EggGroup
    {
        TwoKM = 2,
        FiveKM = 5,
        SevenKM = 7,
        TenKM = 10,
    }
}
