﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minipokedex.Core.Enum
{
    public enum Strength
    {
        Strong=1,
        Weak,
        Neutral
    }
}
