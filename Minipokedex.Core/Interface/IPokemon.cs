﻿using Minipokedex.Core.Model;
using System.Collections.Generic;

namespace Minipokedex.Core.Interface
{
    public interface IPokemon
    {
        IEnumerable<ElementType> GetAllElement();
        IEnumerable<Generation> GetAllGeneration();

        IEnumerable<string> GetPokemonMoves(string guid);

        string GetElementName(string guid);
    }
}
