﻿using Minipokedex.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Minipokedex.Core.Interface
{
    public interface IGenericRepository<T> where T : BaseModel
    {
        Task<T> GetEntityByIdAsync(Guid guid);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> AddEntityAsync(T entityToAdd);
        Task<T> UpdateEntityAsync(T entityToUpdate);
        Task<int> DeleteEntityAsync(T entityToDelete);
        Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> predicate);
        void AddEntityList(IEnumerable<T> listOfEntity);
    }
}
