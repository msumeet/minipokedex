﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Minipokedex.Core.Model
{
    public class PokemonMoves : BaseModel
    {
        [ForeignKey("Pokemon")]
        public Guid PokemonId { get; set; }
        public virtual Pokemon Pokemon { get; set; }

        [ForeignKey("MoveSet")]
        public Guid MoveSetId { get; set; }
        public virtual MoveSet MoveSet { get; set; }
    }
}
