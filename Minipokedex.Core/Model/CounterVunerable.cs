﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Minipokedex.Core.Model
{
    public class CounterVunerable : BaseModel
    {
        public virtual Pokemon ForPokemon { get; set; }
        public virtual ICollection<Pokemon> Weakness { get; set; }
        public virtual ICollection<Pokemon> Strength { get; set; }
        public string Efficiency { get; set; }
    }
}
