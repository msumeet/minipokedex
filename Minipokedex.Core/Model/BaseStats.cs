﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Minipokedex.Core.Model
{
    public class BaseStats : BaseModel
    {
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int Stamina { get; set; }
        public int MaxCombatPower { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
    }
}
