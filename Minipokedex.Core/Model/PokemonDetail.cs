﻿using Minipokedex.Core.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Minipokedex.Core.Model
{
    public class PokemonDetail : BaseModel
    {
        public int TotalEvolution { get; set; }
        public string Info { get; set; }
        [ForeignKey("BaseStats")]
        public Guid BaseStatsId { get; set; }
        public virtual BaseStats BaseStats { get; set; }
        public BuddyDistance BuddyDistance { get; set; }
        public Obtainability Obtainability { get; set; }
        public EggGroup EggGroup { get; set; }
        public string Family { get; set; }
        public int EvolutionStage { get; set; }
    }
}
