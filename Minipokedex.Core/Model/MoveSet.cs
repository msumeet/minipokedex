﻿using Minipokedex.Core.Enum;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Minipokedex.Core.Model
{
    public class MoveSet : BaseModel
    {
        public string MoveName { get; set; }
        public string Damage { get; set; }
        public decimal DamagePerSecond { get; set; }

        [ForeignKey("ElementType")]
        public Guid ElementTypeId { get; set; }
        public virtual ElementType ElementType { get; set; }
        public MoveType MoveType { get; set; } //Quick or Charge
    }
}
