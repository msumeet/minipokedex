﻿namespace Minipokedex.Core.Model
{
    public class ElementType : BaseModel
    {
        public string ElementName { get; set; }
        public string ColorCode { get; set; }
    }
}
