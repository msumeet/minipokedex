﻿namespace Minipokedex.Core.Model
{
    public class Generation : BaseModel
    {
        public string GenerationName { get; set; }
        public int TotalPokemon { get; set; }
    }
}
