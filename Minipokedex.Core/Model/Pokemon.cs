﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Minipokedex.Core.Model
{
    public class Pokemon : BaseModel
    {
        public int PokemonId { get; set; }
        public string PokemonName { get; set; }
        [ForeignKey("Generation")]
        public Guid GenerationId { get; set; }
        public virtual Generation Generation { get; set; }
        [ForeignKey("ElementType")]
        public Guid ElementPrimary { get; set; }
        [ForeignKey("ElementType")]
        public Guid? ElementSecondary { get; set; }
        [ForeignKey("PokemonDetail")]
        public Guid PokemonDetailId { get; set; }
        public virtual PokemonDetail PokemonDetail { get; set; }
        public string AssetsLink { get; set; }
    }
}
