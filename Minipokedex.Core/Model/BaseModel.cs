﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minipokedex.Core.Model
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int Status { get; set; }
    }
}
