import { Component, OnInit } from '@angular/core';
import { DataEntryPokemonService } from '../_services/data-entry-pokemon.service';

@Component({
  selector: 'app-viewmoveset',
  templateUrl: './viewmoveset.component.html',
  styleUrls: ['./viewmoveset.component.css']
})
export class ViewmovesetComponent implements OnInit {
  moveSetList: any;
  elementList: any;
  asset: any;
  constructor(private _depService: DataEntryPokemonService) { }

  ngOnInit() {
    this.getAllMoveSet();
  }

  getAllMoveSet() {
    this._depService.getAllMoveSet().subscribe(x => {
      this.moveSetList = x;
      console.log(this.moveSetList);
    })
    this._depService.getAllElement().subscribe(x => {
      this.elementList = x;
      console.log(this.elementList);
    })
  }
}
