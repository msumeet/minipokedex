import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmovesetComponent } from './viewmoveset.component';

describe('ViewmovesetComponent', () => {
  let component: ViewmovesetComponent;
  let fixture: ComponentFixture<ViewmovesetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmovesetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmovesetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
