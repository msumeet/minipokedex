import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { GenOneComponent } from './region/gen-one/gen-one.component';
import { GenTwoComponent } from './region/gen-two/gen-two.component';
import { GenThreeComponent } from './region/gen-three/gen-three.component';
import { RouteModule } from './_routes/route.module';
import { RegionComponent } from './region/region.component';
import { DataEntryComponent } from './data-entry/data-entry.component';
import { PokemonComponent } from './data-entry/pokemon/pokemon.component';
import { MovesetComponent } from './data-entry/moveset/moveset.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ViewmovesetComponent } from './viewmoveset/viewmoveset.component';
import { PokemonMovesetComponent } from './data-entry/pokemon-moveset/pokemon-moveset.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    GenOneComponent,
    GenTwoComponent,
    GenThreeComponent,
    RegionComponent,
    DataEntryComponent,
    PokemonComponent,
    MovesetComponent,
    ViewmovesetComponent,
    PokemonMovesetComponent,
  ],
  imports: [
    BrowserModule,
    RouteModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
