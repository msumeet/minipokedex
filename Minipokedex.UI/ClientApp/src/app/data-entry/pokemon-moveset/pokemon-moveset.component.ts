import { Component, OnInit } from '@angular/core';
import { DataEntryPokemonService } from '../../_services/data-entry-pokemon.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pokemon-moveset',
  templateUrl: './pokemon-moveset.component.html',
  styleUrls: ['./pokemon-moveset.component.css']
})
export class PokemonMovesetComponent implements OnInit {
  moveSetList: any;
  pokemonList: any;

  pokemonMoveList: any = {
    pokemonId: "",
    moveListId: ""
  };
  constructor(private _depService: DataEntryPokemonService, private _toaster: ToastrService) { }

  ngOnInit() {
    this.getMoveSet();
    this.getPokemonList();
  }

  getMoveSet() {
    this._depService.getAllMoveSet().subscribe(x => {
      // debugger
      console.log(x);
      this.moveSetList = x;
    });
  }

  getPokemonList() {
    this._depService.getPokemonList().subscribe(x => {
      this.pokemonList = x;
     
    });
  }

  onSubmit(values: any) {
    console.log(values);
    this._depService.addPokemonMoveList(values).subscribe(x => {
      console.log(x);
      this._toaster.success("Moveset Added", "Success", { timeOut: 3000 });
    }, error => {
      this._toaster.error("Move Couldnot be set", "Error", { timeOut: 2000 });
    });
  }
}