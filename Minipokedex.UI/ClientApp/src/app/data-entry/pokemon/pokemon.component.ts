import { Component, OnInit } from '@angular/core';
import { DataEntryPokemonService } from '../../_services/data-entry-pokemon.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {
  elementList: any;
  generationList: any;
  constructor(private _depService: DataEntryPokemonService, private router: Router, private _toaster: ToastrService) { }

  ngOnInit() {
    this.getGenerationList();
    this.getElementList();
  }

  getElementList() {
    this._depService.getAllElement().subscribe(x => {
      this.elementList = x;
    });
  }
  getGenerationList() {
    this._depService.getAllGeneration().subscribe(x => {
      this.generationList = x;
      debugger
      console.log(this.generationList);
    });
  }

  onSubmit(values: any) {
    debugger;
    if (values.pokemonId > 9) {
      values.assetsLink = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/0" + values.pokemonId + ".png"
    } else if (values.pokemonId > 99) {
      values.assetsLink = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + values.pokemonId + ".png"
    } else {
      values.assetsLink = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/00" + values.pokemonId + ".png"

    }
    console.log(values);
    this._depService.addNewPokemonEntry(values)
      .subscribe(x => {
        this._toaster.success("New Pokemon added Sucessfully", "Success", { timeOut: 3000 });
      }, error => {
        this._toaster.error("Duplicate Entry", "Error", { timeOut: 3000 });
      });
    this.router.navigateByUrl('kanto');
  }
}
