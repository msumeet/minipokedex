import { Component, OnInit } from '@angular/core';
import { DataEntryPokemonService } from '../../_services/data-entry-pokemon.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-moveset',
  templateUrl: './moveset.component.html',
  styleUrls: ['./moveset.component.css']
})
export class MovesetComponent implements OnInit {
  elementList: any;
  moveSetList: any;
  moveType: any;
  color: any;
  constructor(private _depService: DataEntryPokemonService, private router: Router, private _toaster: ToastrService) { }

  ngOnInit() {
    this.getElementList();
    this.getAllMoveSet();
  }
  getElementList() {
    this._depService.getAllElement().subscribe(x => {
      this.elementList = x;
      //  console.log(x);
    });
  }

  onSubmit(values: any) {
    console.log(values);
    this._depService.addNewMoveSet(values).subscribe(x => {
      // console.log(x);
      this._toaster.success("Move set added", "Success", { timeOut: 3000 });
    }, error => {
      this._toaster.error(error, "Error", { timeOut: 3000 });
    });
   // values.resetForm();
    this.router.navigateByUrl('viewmoveset');

  }

  getAllMoveSet() {
    this._depService.getAllMoveSet().subscribe(x => {
      this.moveSetList = x;
      console.log(this.moveSetList);
    })
  }

  setcolor(finder: string) {
    var mvtp = this.elementList.find(x => x.id == finder);
    this.color = "#" + mvtp.colorCode;
    return this.color;
  }

  finder(finder: string) {
    var mvtp = this.elementList.find(x => x.id == finder);
    this.moveType = mvtp.elementName;
    this.color = mvtp.colorCode;
    return this.moveType;
  }
}
