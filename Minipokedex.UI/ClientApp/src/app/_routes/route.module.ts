import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegionComponent } from '../region/region.component';
import { GenOneComponent } from '../region/gen-one/gen-one.component';
import { GenTwoComponent } from '../region/gen-two/gen-two.component';
import { GenThreeComponent } from '../region/gen-three/gen-three.component';
import { DataEntryComponent } from '../data-entry/data-entry.component';
import { PokemonComponent } from '../data-entry/pokemon/pokemon.component';
import { MovesetComponent } from '../data-entry/moveset/moveset.component';
import { ViewmovesetComponent } from '../viewmoveset/viewmoveset.component';
import { PokemonMovesetComponent } from '../data-entry/pokemon-moveset/pokemon-moveset.component';


const routes: Routes = [
  { path: '', component: RegionComponent, pathMatch: 'full' },
  { path: 'kanto', component: GenOneComponent },
  { path: 'johto', component: GenTwoComponent },
  { path: 'hoenn', component: GenThreeComponent },
  {
    path: 'dataentry', component: DataEntryComponent, children: [
      { path: 'pokemon', component: PokemonComponent },
      { path: 'moveset', component: MovesetComponent },
      { path: 'pokemonmoveset', component: PokemonMovesetComponent }

    ]
  },
  { path: 'viewmoveset', component: ViewmovesetComponent },

]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RouteModule { }
export const routing = RouterModule.forRoot(routes);
