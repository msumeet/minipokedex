import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { debug } from 'util';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  //44359 
  urlKanto: string = "https://localhost:44359/api/Pokemon/GetAll";
  urlElement: string = "https://localhost:44359/api/Common/GetAllElem";
  urlPokemonDetail: string = "https://localhost:44359/api/PokemonDetail/GetById/";
  urlBaseStats: string = "https://localhost:44359/api/BaseStats/GetById/";

  urlPokemonMoves: string = "https://localhost:44359/api/Common/GetPokeMove/"
  constructor(private http: HttpClient) { }

  getPokemon(region: string) {

  }

  getPokemonList(region: string): any {
    return this.http.get<any>(this.urlKanto)
      .pipe(
        catchError(this.handleError)
      );
  }

  getElementList(): any {
    return this.http.get(this.urlElement)
      .pipe(
        catchError(this.handleError)
      );
  }

  getPokemonDetail(guid: string) {
    // let params = new HttpParams().set("guid", guid);
    return this.http.get(this.urlPokemonDetail + guid);
  }

  getBaseStats(guid: string) {
    // let params = new HttpParams().set("guid", guid);
    return this.http.get(this.urlBaseStats + guid);
  }

  getPokemonMoveSet(guid: string) {
    // debugger
    return this.http.get(this.urlPokemonMoves + guid)
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(error.error.message)
    } else {
      console.error(error.status)
    }
    return throwError('Something bad happened; please try again later.');
  }
}
