import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataEntryPokemonService {
  urlElement: string = "https://localhost:44359/api/Common/GetAllElem";
  urlGeneration: string = "https://localhost:44359/api/Common/GetAllGen";
  urlGetElement: string = "https://localhost:44359/api/Common/GetElement/";

  urlGetMoveset: string = "https://localhost:44359/api/MoveSet/GetAll";
  urlMoveset: string = "https://localhost:44359/api/MoveSet/Add";

  urlAddPokemon: string = "https://localhost:44359/api/Pokemon/Add";
  urlKantoPokemon: string = "https://localhost:44359/api/Pokemon/GetAll";

  urlGetPokemonMoveSet: string = "https://localhost:44359/api/PokemonMove/GetAll";
  urlAddPokemonMoveSet: string = "https://localhost:44359/api/PokemonMove/Add";

  constructor(private http: HttpClient) { }

  getAllElement() {
    return this.http.get(this.urlElement);
  }

  getAllGeneration() {
    return this.http.get(this.urlGeneration);
  }

  getAllMoveSet() {
    return this.http.get(this.urlGetMoveset);
  }

  addNewPokemonEntry(values: any) {
    return this.http.post(this.urlAddPokemon, values);
  }

  addNewMoveSet(values: any) {
    return this.http.post(this.urlMoveset, values)
      .pipe(
        catchError(this.handleError)
      );
  }

  getPokemonMoveSet() {
    return this.http.get(this.urlGetPokemonMoveSet)
      .pipe(
        catchError(this.handleError)
      );
  }

  getPokemonList(): any {
    return this.http.get(this.urlKantoPokemon)
      .pipe(
        catchError(this.handleError)
      );
  }

  addPokemonMoveList(values: any) {
    return this.http.post(this.urlAddPokemonMoveSet, values)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(error.error.message)
    } else {
      console.error(error.status)
    }
    return throwError('Duplicate data.');
  }
}
