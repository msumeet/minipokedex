import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../_services/pokemon.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gen-one',
  templateUrl: './gen-one.component.html',
  styleUrls: ['./gen-one.component.css']
})
export class GenOneComponent implements OnInit {
  region: string;
  pokemonListResponse: any = [];
  elementListResponse: any = [];
  element: any;
  primaryColor: any;
  pElem: any;
  sElem: any;

  pokemon: any = [];
  pokemonDetail: any = [];
  pokemonBaseStat: any = [];

  moveNames: any;
  constructor(private _pokemonService: PokemonService,
    private _toaster: ToastrService) { }

  ngOnInit() {
    this.getElementList();
    this.getPokemonList();
    // if (this.elementListResponse == null) {
    //   this._toaster.info("No Data", "Info", { timeOut: 5000 })
    // }
  }

  getElementList(): any {
    this._pokemonService.getElementList().subscribe(r => {
      this.elementListResponse = r;
      console.log(this.elementListResponse);
    });
  }

  getPokemonList(): any {
    this.region = "Kanto";
    this._pokemonService.getPokemonList(this.region).subscribe(r => {
      this.pokemonListResponse = r;
      console.log(this.pokemonListResponse);

    });
  }

  findColorP(finder: string) {
    this.findColor(finder);
    this.pElem = this.element.elementName;
    return this.primaryColor;
  }
  findColorS(finder: string) {
    this.findColor(finder);
    this.sElem = this.element.elementName;
    return this.primaryColor;
  }
  findColor(finder: string) {
    this.element = this.elementListResponse.find(x => x.id == finder);
    this.primaryColor = "#" + this.element.colorCode;
  }


  sendDataToModal(pokemon: any) {

    this.pokemon = pokemon;
    console.log(pokemon);
    this._pokemonService.getPokemonDetail(pokemon.pokemonDetailId).subscribe(x => {
      console.log(x);
      this.pokemonDetail = x;
      this._pokemonService.getBaseStats(this.pokemonDetail.baseStatsId).subscribe(y => {
        this.pokemonBaseStat = y;
        console.log(y);
      });
     // debugger
      this._pokemonService.getPokemonMoveSet(this.pokemon.id).subscribe(z => {
        console.log(z);
        // debugger
        this.moveNames = z;
      })
    }, error => {

    });

  }
}
